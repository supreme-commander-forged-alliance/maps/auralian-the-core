version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Auralian - The Core",
    description = "An human-made island which turned out to be quite... resourceful.",
    preview = '',
    map_version = 16,
    type = 'skirmish',
    starts = true,
    size = {256, 256},
    reclaim = {12381.03, 28688.77},
    map = '/maps/auralian_-_the_core_core/auralian_-_the_core.scmap',
    save = '/maps/auralian_-_the_core_core/auralian_-_the_core_save.lua',
    script = '/maps/auralian_-_the_core_core/auralian_-_the_core_script.lua',
    norushradius = 0,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_9 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
