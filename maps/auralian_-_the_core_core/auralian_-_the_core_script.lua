local Behaviors = import('/lua/ai/opai/OpBehaviors.lua')
local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioPlatoonAI = import('/lua/ScenarioPlatoonAI.lua')
local ScenarioStrings = import('/lua/ScenarioStrings.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')


function OnPopulate()

  --ScenarioFramework.Dialogue(OpStrings.A01_M03_020);
  --ScenarioFramework.Dialogue(import('/maps/SCCA_A01/SCCA_A01_strings.lua').A01_M01_010 , nil, true);

  ScenarioUtils.InitializeArmies();

  SetupVision();

end

function SetupVision()

		-- The variable ArmyBrains represents a player. I don't know why and I don't know how, but this works.

		-- Code from SCA_A03_Script:
		  --    ScenarioInfo.UEFViz = ScenarioFramework.CreateVisibleAreaLocation(100,
		  --    ScenarioUtils.MarkerToPosition('Player'), 0, ArmyBrains[uef])

		-- Gives vision on the battleship wreckage in the middle of the map.
  --ScenarioFramework.CreateVisibleAreaLocation(25, ScenarioUtils.MarkerToPosition('Visibility_Mid'), 60, ArmyBrains[1]);
  --ScenarioFramework.CreateVisibleAreaLocation(25, ScenarioUtils.MarkerToPosition('Visibility_Mid'), 60, ArmyBrains[2]);

  ScenarioFramework.CreateVisibleAreaLocation(30, ScenarioUtils.MarkerToPosition('Visibility_Mid'), 60, ArmyBrains[1]);
  ScenarioFramework.CreateVisibleAreaLocation(30, ScenarioUtils.MarkerToPosition('Visibility_Mid'), 60, ArmyBrains[2]);

  	-- Gives vision on the easy mass (rocks), both players only see their own rocks.

  ScenarioFramework.CreateVisibleAreaLocation(10, ScenarioUtils.MarkerToPosition('Visiblity_ARMY_1_MASS_1'), 40, ArmyBrains[1]);
  ScenarioFramework.CreateVisibleAreaLocation(10, ScenarioUtils.MarkerToPosition('Visiblity_ARMY_1_MASS_2'), 40, ArmyBrains[1]);

  ScenarioFramework.CreateVisibleAreaLocation(10, ScenarioUtils.MarkerToPosition('Visiblity_ARMY_2_MASS_1'), 40, ArmyBrains[2]);
  ScenarioFramework.CreateVisibleAreaLocation(10, ScenarioUtils.MarkerToPosition('Visiblity_ARMY_2_MASS_2'), 40, ArmyBrains[2]);

    -- Gives vision on the 'bases' of the civilians. Both players only see their own base.
    -- These no longer exist.

  --ScenarioFramework.CreateVisibleAreaLocation(20, ScenarioUtils.MarkerToPosition('Visiblity_ARMY_1'), 40, ArmyBrains[1]);
  --ScenarioFramework.CreateVisibleAreaLocation(20, ScenarioUtils.MarkerToPosition('Visiblity_ARMY_2'), 40, ArmyBrains[2]);


end

function SetupPatrol()

  -- Alright, this is quite more tricky:

    --factory = ScenarioInfo.UnitNames[UEF]['M6_UEFBase_Factory']
    --engineers = ScenarioUtils.CreateArmyGroup ( 'UEF', 'M6_UEFBase_EngineersHard' )
    --for k,v in engineers do
      --IssueGuard({v}, factory)

   LOG("THIS IS A LOG");

  --scout = ScenarioInfo.UnitNames[UEF]['ScoutA'];
  --LOG(scout);
  --LOG("Scout Logged");
  --

  Scouts = ScenarioUtils.CreateArmyGroup( 'ARMY_9', 'Scouts');

  LOG("Scouts made");

    for k,v in Scouts do

      IssuePatrol({v}, ScenarioUtils.MarkerToPosition('Scout_A_Patrol_1'));
      IssuePatrol({v}, ScenarioUtils.MarkerToPosition('Scout_A_Patrol_2'));

      LOG("Scout is flying!");
    end

end

function OnStart(self)

  SetupPatrol();

end