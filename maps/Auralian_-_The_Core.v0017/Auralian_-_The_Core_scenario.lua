version = 3
ScenarioInfo = {
    name = 'Auralian - The Core',
    description = '  ',
    type = 'skirmish',
    starts = true,
    preview = '',
    size = {256, 256},
    map = '/maps/Auralian_-_The_Core.v0017/Auralian_-_The_Core.scmap',
    save = '/maps/Auralian_-_The_Core.v0017/Auralian_-_The_Core_save.lua',
    script = '/maps/Auralian_-_The_Core.v0017/Auralian_-_The_Core_script.lua',
    norushradius = 40.000000,
    norushoffsetX_ARMY_1 = 0.000000,
    norushoffsetY_ARMY_1 = 0.000000,
    norushoffsetX_ARMY_2 = 0.000000,
    norushoffsetY_ARMY_2 = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'ARMY_1','ARMY_2',} },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    }}
