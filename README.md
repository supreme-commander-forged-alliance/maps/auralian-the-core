## Auralian - The Core

One of my very first maps that is remade with todays knowledge and experience.

![](images/preview-1.png)

## Statistics on the map

The map is designed for casual play. It can be a great introduction map against an AI as there are a few angles of attack. There is reclaim in the center to learn the player how important reclaim can be.

There is reclaim on this map. This includes: 
 - A large reclaim field in the center.
 - Numerous of trees and tree groups.

![](images/preview-2.png)

## Technical aim of the map

This map was an experiment for learning about how to make different rock / cliff formations. In turn I learned about the Multi-Chooser device in World Machine and various interesting properties of the Erosion device. I finally found a reason to use the global intensifier: anything that is underwater!

## Installation guide

The map is available in the [FAF](https://www.faforever.com/) map vault.

For the FAF-version store the map in the following folder:
 - %USER%/My Documents/My Games/Gas Powered Games/Supreme Commander Forged Alliance/maps

If the map folder does not exist in then you can freely make one with the corresponding name.

## Map preview

![](images/Auralian_-_The_Core_scenario_preview.png)

## License

All textures in /env/layers are from www.textures.com. I am obligated to add this text:

One or more textures bundled with this project have been created with 
images from Textures.com. These images may not be redistributed by 
default. Please visit www.textures.com for more information.

All other work is licensed under CC BY-NC-ND 4.0. For more information:
 - https://creativecommons.org/licenses/by-nc-nd/4.0/
 - https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
